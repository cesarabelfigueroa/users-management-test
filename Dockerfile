FROM python:3.8-slim

WORKDIR /app

COPY  . .

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Expose the port where FastAPI application runs
EXPOSE 8000

# Command to run the application with Uvicorn
CMD ["python", "main.py"]
