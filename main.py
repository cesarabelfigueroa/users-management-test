"""
    list model
"""
from typing import List
from fastapi import FastAPI
from databases import Database
from sqlalchemy import create_engine, Column, Integer, String, MetaData, Table
from sqlalchemy.sql import select
from pydantic import BaseModel
import requests



app = FastAPI()

# Define the SQLite database URL
DATABASE_URL = "sqlite:///./test.db"

# Create a Database instance
database = Database(DATABASE_URL)

# Define metadata for SQLAlchemy
metadata = MetaData()

# Define the "users" table
users = Table(
    "users",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("username", String, unique=True, index=True),
    Column("email", String, unique=True, index=True),
)

# Create the SQLAlchemy engine
engine = create_engine(DATABASE_URL, connect_args={"check_same_thread": False})

# Create the "users" table in the database
metadata.create_all(engine)

class UserCreate(BaseModel):
    """
        user model
    """
    username: str
    email: str



class UserResponse(BaseModel):
    """
        users response model
    """
    id: int
    username: str
    email: str

@app.post("/users/", response_model=UserResponse)
async def create_user(user: UserCreate):
    """
        function to create user
    """
    query = users.insert().values(username=user.username, email=user.email)
    user_id = await database.execute(query)
    return {"id": user_id, **user.dict()}

@app.get("/users/", response_model=List[UserResponse])
async def list_users():
    """
        list users
    """
    query = select(users)
    return await database.fetch_all(query)

@app.get("/slo/")
async def get_slo():
    """
        function to list the SLO from Dynatrace
    """
    path = "https://pcp18257.live.dynatrace.com/api/v2/slo/"
    token = "dt0c01.NZIM3CBTBR4GS5T2U2GRXBRH"
    sufix  = "QS6QCHQWOIYOZJJPM73FRVJSMD2MWCSWYTNHCYRYSMCTQ6Z2WJSLVMFR34FI3EYQ"
    headers = {
        "Authorization": "Api-Token " + token + sufix
    }
    response = requests.get(path, headers=headers, timeout=100)
    return response.json()



@app.on_event("startup")
async def startup_db():
    """
        init the database
    """
    await database.connect()

@app.on_event("shutdown")
async def shutdown_db():
    """
        shutdown the dabase
    """
    await database.disconnect()

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
    