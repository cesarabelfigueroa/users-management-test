import pytest
from fastapi.testclient import TestClient
from main import app, database, metadata, users

client = TestClient(app)

@pytest.fixture
def test_user():
    return {"username": "testuser", "email": "test@example.com"}

def test_create_user(test_user):
    # Test creating a user
    response = client.post("/users/", json=test_user)
    assert response.status_code == 200
    assert response.json()["username"] == test_user["username"]
    assert response.json()["email"] == test_user["email"]

def test_list_users():
    # Test listing users
    response = client.get("/users/")
    assert response.status_code == 200
    assert isinstance(response.json(), list)
